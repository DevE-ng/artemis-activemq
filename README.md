# activemq

example of pubsub to artemis activemq 

git clone
change application.properties
gradle build

SH script example

```
#!/bin/sh
CLASS_PATH="path to jar, get it from build/libs"
PID=activemq.pid

/opt/ActiveMQ/JDK/jdk-19.0.1/bin/java -jar  -Dspring.pid.file="$PID" $CLASS_PATH &>/home/oracle/jms/activemq_log.txt
```


http://server:8888/test?message=abcc
will send mesage to a broker, and two subscribers will get message, will be registered in log file