package com.spring.activemq.pubsub.controllers;

import com.spring.activemq.pubsub.publisher.MessagePublisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestMessage {
    @Autowired
    private MessagePublisher messagePublisher;

    @GetMapping("/test")
    public ResponseEntity<String> test(@RequestParam(defaultValue = "no details", required=false) String message) {
        String json = String.format("{msg_id:1,flex_client_id:123, gl_id:null, message:%s }", message);
        messagePublisher.sendMessage(json);
        return ResponseEntity.ok("Message has been sent: " + json);

    }

}